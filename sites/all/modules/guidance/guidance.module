<?php


/**
 * Guidance module
 * Administer can define guides.
 * A guide contains a bunch of links the visitor should walk through.
 * Visited links are signed.
 */


define('GUIDANCE_ADMIN_PERMISSION', 'administer guidance blocks');
define('GUIDANCE_GUIDE_ON', '1');
define('GUIDANCE_GUIDE_OFF', '0');


/**
 * Implementation of hook_menu().
 * @return array
 */
function guidance_menu() {
  // List guide's links
  $items['node/%/guidance'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Guide links',
    'title callback' => 't',
    'page callback' => 'guidance_guide_page',
    'page arguments' => array(1),
    'access callback' => 'guidance_guidance_node_access',
    'access arguments' => array(GUIDANCE_ADMIN_PERMISSION, 1),
    'file' => 'guidance.admin.inc',
  );

  $items['node/%/guidance/list'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'Guide links',
    'weight' => 0,
  );

  // Add new link
  $items['node/%/guidance/add'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Add link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guidance_link_form', 1),
    'access callback' => 'user_access',
    'access arguments' => array(GUIDANCE_ADMIN_PERMISSION),
    'file' => 'guidance.admin.inc',
    'weight' => 10,
  );

  // Edit link
  $items['node/%/guidance/%/edit'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guidance_link_form', 1, 3),
    'access callback' => 'user_access',
    'access arguments' => array(GUIDANCE_ADMIN_PERMISSION),
    'file' => 'guidance.admin.inc',
  );

  // Delete link
  $items['node/%/guidance/%/delete'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guidance_link_delete_form', 3),
    'access callback' => 'user_access',
    'access arguments' => array(GUIDANCE_ADMIN_PERMISSION),
    'file' => 'guidance.admin.inc',
  );

  // Show guide page through redirect
  $items['guide/%/%'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'guidance_link_redirect',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
    'file' => 'guidance.page.inc',
  );

  return $items;
}


/**
 * Implementation of hook_perm().
 *
 * @return array
 */
function guidance_permission() {
	return array(
    GUIDANCE_ADMIN_PERMISSION => array(
      'title' => t('Administer Guidance'),
      'description' => t('Create and update guidance contents.'),
    ),
  );
}


/**
 * Implementation of hook_init().
 */
function guidance_init() {
  drupal_add_css(drupal_get_path('module', 'guidance').'/guidance.css');
}


/**
 * Implemntation of hook_node_info().
 * Define node type: guidance.
 * @return array
 */
function guidance_node_info() {
  return array(
    'guidance' => array(
      'name' => t('Guide'),
      'base' => 'node_content',
      'description' => t('Define a content that can contain guide links. Special content type.'),
    ),
  );
}


/**
 * Implementation of hook_block_info().
 * 
 * @return string
 */
function guidance_block_info() {
  $guides = guidance_get_guides();
  $blocks = array();
  foreach ($guides as $guide) {
    $blocks[$guide->nid * 2]['info'] = t('Guide: @title',
      array('@title' => _guidance_title_format($guide->title, '#'.$guide->nid)));
    $blocks[$guide->nid * 2]['cache'] = DRUPAL_NO_CACHE;
    $blocks[$guide->nid * 2 - 1]['info'] = t('Guide runner for: @title',
      array('@title' => _guidance_title_format($guide->title, '#'.$guide->nid)));
    $blocks[$guide->nid * 2 - 1]['cache'] = DRUPAL_NO_CACHE;
  }
  $blocks[0] = array(
    'info' => t('Guide description - displays the current guide link\'s description.'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}


/**
 * Implementation of hook_block_view().
 *
 * @param int $delta
 * @return array
 */
function guidance_block_view($delta = 0) {
  $blocks = array();
  if ($delta == 0) {
    if ($_SESSION['guide_last_visited_lifetime']-- > 0) {
      // Description block
      $lid = $_SESSION['guide_last_visited'];
      $link = guidance_get_guide_link($lid);
      $blocks['subject'] = t('Guide help:');
      $blocks['content'] = $link->description;
      return $blocks;
    }
  } else if ($delta & 1) {
    // Guide runner
    module_load_include('inc', 'guidance', 'guidance.block');
    return guidance_create_runner_block(($delta + 1) / 2);
  } else {
    // Guide block
    module_load_include('inc', 'guidance', 'guidance.block');
    return guidance_create_block($delta / 2);
  }
}


/**
 * Implementation of hook_forms().
 * @return array
 */
function guidance_forms() {
  $results = db_query('SELECT nid FROM {node} WHERE type = \'guidance\';');
  $form = array();
  foreach ($results as $result) {
    $form['guidance_block_runner_form_'.$result->nid] = array(
      'callback' => 'guidance_block_runner_form',
    );
    $form['guidance_block_closer_form_'.$result->nid] = array(
      'callback' => 'guidance_block_closer_form',
    );
  }
  return $form;
}


/**
 * Get each guide defined
 * @return array
 */
function guidance_get_guides() {
  return db_query('
    SELECT n.nid, n.title
    FROM {node} n
    WHERE type = \'guidance\'
    GROUP BY n.nid
    ORDER BY n.nid DESC
  ');
}


/**
 * Get a guide
 * @param int $nid
 * @return object
 */
function guidance_get_guide($nid) {
 	return node_load($nid);
}


/**
 * Get a guide's links
 * @param int $nid
 * @return array
 */
function guidance_get_guide_links($nid) {
  $resource = db_query('SELECT * FROM {guidance_links} WHERE nid = :nid ORDER BY weight ASC;', array(':nid' => $nid));

  $results = array();
  foreach ($resource as $record) {
		if (!empty($record->callback) && function_exists($record->callback)) {
			$record->visited = call_user_func($record->callback, $record);
		} else {
    	$record->visited = guidance_is_page_visited($record);
		}
    $results[] = $record;
  }

  return $results;
}


/**
 * Get a link.
 *
 * @param int $lid
 * @return object
 */
function guidance_get_guide_link($lid) {
  static $link = NULL;
  if (empty($link)) {
    $link = db_query('SELECT * FROM {guidance_links} WHERE lid = :lid;', array(':lid' => $lid))->fetchObject();
  }
  return $link;
}


/**
 * Get correct title string format.
 * @param string $title
 * @param string $substite
 * @return string
 */
function _guidance_title_format($title, $substitute) {
  return check_plain(empty($title) ? $substitute : $title);
}


/**
 * Save that a user visits a guide link
 * @param int $lid
 */
function guidance_save_visit($link) {
  if (empty($_SESSION['guide'][$link->nid])) {
    $_SESSION['guide'][$link->nid] = array();
  }

  $_SESSION['guide_last_visited'] = $link->lid;
  $_SESSION['guide_last_visited_lifetime'] = 1;

  $_SESSION['guide'][$link->nid][] = $link->lid;
}


/**
 * Save the state of a guide. (Running or closed.)
 */
function guidance_save_state($nid, $state = GUIDANCE_GUIDE_OFF) {
	$_SESSION['guidance_state'][$nid] = $state;
	if ($state == GUIDANCE_GUIDE_OFF) {
		$_SESSION['guide'][$nid] = array();
		// @TODO think about the external functions
	}
}


/**
 * Check whether the guide is run.
 */
function guidance_is_run($nid) {
	return isset($_SESSION['guidance_state'][$nid]) && $_SESSION['guidance_state'][$nid] == GUIDANCE_GUIDE_ON;
}


/**
 * Check whether a guide link is visited.
 *
 * @param array $link
 * @return boolean
 */
function guidance_is_page_visited($link) {
  return isset($link->lid) &&
         isset($_SESSION['guide'][$link->nid]) &&
         in_array($link->lid, (array)$_SESSION['guide'][$link->nid]);
}


/**
 * Implementation of hook_theme().
 * @return array
 */
function guidance_theme() {
  return array(
    'guidance_guide_links_form' => array(
      'render element' => 'form',
      'file' => 'guidance.admin.inc',
    ),
  );
}


/**
 * Implementation of hook_node_delete().
 *
 * Deleting unused links.
 *
 * @param object $node
 */
function guidance_node_delete($node) {
  db_query('DELETE FROM {guidance_links} WHERE nid = :nid;', array(':nid' => $node->nid));
}


/**
 * Restrict the guide tab menu for the guidance content types.
 */
function guidance_guidance_node_access($permission, $nid) {
  $node = node_load($nid);
  return user_access($permission) && $node->type == 'guidance';
}


/**
 * Implementation of hook_guidance_callback().
 */
function guidance_guidance_callback() {
	$callbacks['guidance_is_page_visited'] = array(
		'info' => t('URL is visited.'),
		'help' => t('The callback checks if the url given by the link is visited. It only counts when the user clicks on the guide link.'),
	);
	return $callbacks;
}


function guidance_set_breadcrumb($nid = NULL, $lid = NULL) {
  $breadcrumbs = array(l(t('Home'), '<front>'));

  if (!empty($nid)) {
    $node = node_load($nid);
    $breadcrumbs[] = l(t('Guide links (@title)', array('@title' => $node->title)), 'node/' . $nid . '/guidance/list');
  }

  if (!empty($lid)) {
    $link = guidance_get_guide_link($lid);
    $breadcrumbs[] = l(t('Link (@title)', array('@title' => $link->title)), 'node/' . $nid . '/guidance/' . $lid . '/edit');
  }

  drupal_set_breadcrumb($breadcrumbs);
}

