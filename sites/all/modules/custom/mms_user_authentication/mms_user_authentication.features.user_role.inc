<?php
/**
 * @file
 * mms_user_authentication.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mms_user_authentication_user_default_roles() {
  $roles = array();

  // Exported role: unverified user.
  $roles['unverified user'] = array(
    'name' => 'unverified user',
    'weight' => 3,
  );

  return $roles;
}
