<?php
/**
 * @file
 * mms_user_authentication.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mms_user_authentication_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
