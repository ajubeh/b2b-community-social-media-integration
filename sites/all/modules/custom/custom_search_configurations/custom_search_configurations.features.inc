<?php
/**
 * @file
 * custom_search_configurations.features.inc
 */

/**
 * Implements hook_default_search_api_index().
 */
function custom_search_configurations_default_search_api_index() {
  $items = array();
  $items['database_node_index'] = entity_import('search_api_index', '{
    "name" : "Database node Index",
    "machine_name" : "database_node_index",
    "description" : null,
    "server" : "database_search_server",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "150",
      "fields" : {
        "type" : { "type" : "string" },
        "title" : { "type" : "text" },
        "status" : { "type" : "boolean" },
        "created" : { "type" : "date" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "comment_count" : { "type" : "integer" },
        "field_label" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "taxonomy_forums" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_knowledge_base_article" : { "type" : "integer", "entity_type" : "node" },
        "field_answer_question" : { "type" : "integer", "entity_type" : "node" },
        "group_group" : { "type" : "boolean" },
        "search_api_language" : { "type" : "string" },
        "search_api_url" : { "type" : "uri" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function custom_search_configurations_default_search_api_server() {
  $items = array();
  $items['database_search_server'] = entity_import('search_api_server', '{
    "name" : "Database Search Server",
    "machine_name" : "database_search_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : 1,
      "indexes" : { "database_node_index" : {
          "type" : {
            "table" : "search_api_db_database_node_index",
            "column" : "type",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_database_node_index_title",
            "type" : "text",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_database_node_index",
            "column" : "status",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_database_node_index",
            "column" : "created",
            "type" : "date",
            "boost" : "1.0"
          },
          "author" : {
            "table" : "search_api_db_database_node_index",
            "column" : "author",
            "type" : "integer",
            "boost" : "1.0"
          },
          "comment_count" : {
            "table" : "search_api_db_database_node_index",
            "column" : "comment_count",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_label" : {
            "table" : "search_api_db_database_node_index",
            "column" : "field_label",
            "type" : "integer",
            "boost" : "1.0"
          },
          "taxonomy_forums" : {
            "table" : "search_api_db_database_node_index",
            "column" : "taxonomy_forums",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_knowledge_base_article" : {
            "table" : "search_api_db_database_node_index",
            "column" : "field_knowledge_base_article",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_answer_question" : {
            "table" : "search_api_db_database_node_index",
            "column" : "field_answer_question",
            "type" : "integer",
            "boost" : "1.0"
          },
          "group_group" : {
            "table" : "search_api_db_database_node_index",
            "column" : "group_group",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_database_node_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "search_api_url" : {
            "table" : "search_api_db_database_node_index",
            "column" : "search_api_url",
            "type" : "uri",
            "boost" : "1.0"
          },
          "search_api_access_node" : {
            "table" : "search_api_db_database_node_index_search_api_access_node",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
