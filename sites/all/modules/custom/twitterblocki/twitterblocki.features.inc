<?php
/**
 * @file
 * twitterblocki.features.inc
 */

/**
 * Implements hook_views_api().
 */
function twitterblocki_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
