<?php
/**
 * @file
 * flags.features.inc
 */

/**
 * Implements hook_flag_default_flags().
 */
function flags_flag_default_flags() {
  $flags = array();
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'entity_type' => 'node',
    'title' => 'Bookmarks',
    'global' => 0,
    'types' => array(
      0 => 'article',
      1 => 'forum',
      2 => 'blog',
    ),
    'flag_short' => 'Bookmark this',
    'flag_long' => 'Add this post to your bookmarks',
    'flag_message' => 'This post has been added to your bookmarks',
    'unflag_short' => 'Unbookmark this',
    'unflag_long' => 'Remove this post from your bookmarks',
    'unflag_message' => 'This post has been removed from your bookmarks',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -11,
    'show_in_links' => array(
      'full' => 1,
      'teaser' => 1,
    ),
    'show_as_field' => FALSE,
    'show_on_form' => 1,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "follow user".
  $flags['follow_user'] = array(
    'entity_type' => 'user',
    'title' => 'follow user',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Folgen',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Folge ich',
    'unflag_long' => '',
    'unflag_message' => 'Folge ich nicht',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Report Comment".
  $flags['kommentare_melden'] = array(
    'entity_type' => 'comment',
    'title' => 'Report Comment',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Melden',
    'flag_long' => 'Spam oder unangebrachten Content melden.',
    'flag_message' => 'Erfolg',
    'unflag_short' => 'Meldung widerrufen',
    'unflag_long' => 'Widerrufe deine Meldung',
    'unflag_message' => 'Erfolg',
    'unflag_denied_text' => 'Nur Admins können eine Meldung widerrufen.',
    'link_type' => 'confirm',
    'weight' => -6,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'flag_confirmation' => 'Bist du sicher das du diesen Inhalt melden möchtest?',
    'unflag_confirmation' => 'Bist du sicher das du die Meldung widerrufen möchtest?',
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Like Node".
  $flags['like'] = array(
    'entity_type' => 'node',
    'title' => 'Like Node',
    'global' => 1,
    'types' => array(
      0 => 'article',
      1 => 'forum',
    ),
    'flag_short' => 'Upvote',
    'flag_long' => 'Upvote <em>[node:title]</em>',
    'flag_message' => '',
    'unflag_short' => 'Downvote',
    'unflag_long' => 'Downvote <em>[node:title]</title>',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -9,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'others',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Like Comment".
  $flags['like_comment'] = array(
    'entity_type' => 'comment',
    'title' => 'Like Comment',
    'global' => 1,
    'types' => array(
      0 => 'comment_node_article',
      1 => 'comment_node_forum',
    ),
    'flag_short' => 'Upvote',
    'flag_long' => 'Upvote <em>[comment:author]\'s</em> Kommentar',
    'flag_message' => '',
    'unflag_short' => 'Downvote',
    'unflag_long' => 'Downvote <em>[comment:author]\'s</em> Kommentar',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -10,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'comment_others',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Report Forum Entry".
  $flags['melden'] = array(
    'entity_type' => 'node',
    'title' => 'Report Forum Entry',
    'global' => 0,
    'types' => array(
      0 => 'forum',
    ),
    'flag_short' => 'Melden',
    'flag_long' => 'Beitrag als unangebracht melden',
    'flag_message' => 'Efolg',
    'unflag_short' => 'Widerrufen',
    'unflag_long' => 'Meldung Widerrufen',
    'unflag_message' => 'Erfolg',
    'unflag_denied_text' => 'Nur Admins können eine Meldung widerrufen.',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'flag_confirmation' => 'Bist du sicher das du diesen Inhalt melden möchtest?',
    'unflag_confirmation' => 'Bist du sicher das du die Meldung widerrufen möchtest?',
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Nominate to Knowledgebase".
  $flags['nominate_to_knowledgebase'] = array(
    'entity_type' => 'comment',
    'title' => 'Nominate to Knowledgebase',
    'global' => 0,
    'types' => array(
      0 => 'comment_node_article',
      1 => 'comment_node_forum',
      2 => 'comment_node_question',
    ),
    'flag_short' => 'Wissensspeicher',
    'flag_long' => 'Nominiere <em>[node:title]</em> für den Wissensspeicher.',
    'flag_message' => '',
    'unflag_short' => 'Nominiert',
    'unflag_long' => 'Ziehe deine Nominierung zurück.',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'comment_others',
    'show_contextual_link' => FALSE,
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Report User".
  $flags['report_user'] = array(
    'entity_type' => 'user',
    'title' => 'Report User',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Melden',
    'flag_long' => 'melde <em>[user]</em>',
    'flag_message' => '<em>[user]</em> wurde gemeldet!',
    'unflag_short' => 'Meldung zurückziehen',
    'unflag_long' => 'ziehe die Meldung zurück',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => -7,
    'show_in_links' => array(
      'full' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'flag_confirmation' => 'Willst du diesen Nutzer wirklich melden?',
    'unflag_confirmation' => 'Willst du deine Meldung wirklich zurückziehen?',
    'api_version' => 3,
    'module' => 'flags',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
