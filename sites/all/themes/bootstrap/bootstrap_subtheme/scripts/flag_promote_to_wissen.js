

jQuery(document).ready(function($) {
	var flagged = $('li.flag-outer-aufgenommen-in-den-wissensspeich:has("a.unflag-action")');
	$(flagged).addClass('flagged');
	$(flagged).parent().parent().addClass('promotedToWissen');
	$('div#promotetowissenflag').parent().addClass('promotedToWissen');
	var beitrag = $('.promotedToWissen');
	
	var bestbeitrag = $('div#promotetowissenflag');
	if (bestbeitrag.length > 0) {
    		$('li.:has("a.flag-action")').hide();
		$('#comments').before('<div id="promotetowissenblock">');
		$('#promotetowissenblock').prepend(answer);
	
	}
	$('li.flag-outer-aufgenommen-in-den-wissensspeich').find('a').click(function(){

		if ($(this).hasClass('unflag-action')) {
			$('li.flag-outer-aufgenommen-in-den-wissensspeich').show();
			$(this).parent().parent().parent().removeClass('flagged');
			$(this).parent().parent().parent().parent().parent().removeClass('promotedToWissen');
		}
		else {
			var clicked = $(this).parent().parent().parent();
			$(clicked).addClass('flagged');
			$('li.flag-outer-aufgenommen-in-den-wissensspeich').not(clicked).hide();
			$(clicked).parent().parent().addClass('promotedToWissen');
		}

	});
	
	$(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
		
		location.reload();
		$('li.flag-outer-aufgenommen-in-den-wissensspeich').find('a').click(function(){

		if ($(this).hasClass('unflag-action')) {
			$('li.flag-outer-aufgenommen-in-den-wissensspeich').show();
			$(this).parent().parent().parent().removeClass('flagged');
			$(this).parent().parent().parent().parent().parent().removeClass('promtedToWissen');
		}
		else {
			var clicked = $(this).parent().parent().parent();
			$(clicked).addClass('flagged');
			$(clicked).parent().parent().addClass('promotedToWissen');
			$('li.flag-outer-aufgenommen-in-den-wissensspeich').not(clicked).hide();
		}

		});
		
		
	});
});





