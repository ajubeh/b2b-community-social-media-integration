

jQuery(document).ready(function($) {
	var flagged = $('li.flag-als_richtige_antwort_markieren:has("a.unflag-action")');
	$(flagged).addClass('flagged');
	$(flagged).parent().parent().addClass('rightAnswer');
	$('div#bestanswerflag').parent().addClass('rightAnswer');
	var answer = $('.rightAnswer');
	
	var bestanswer = $('div#bestanswerflag');
	if (bestanswer.length > 0) {
		$('#comments').before('<div id="rightanswerblock">');
		$('#rightanswerblock').prepend(answer);
	
	}
	$('li.flag-als_richtige_antwort_markieren').find('a').click(function(){

		if ($(this).hasClass('unflag-action')) {
			$('li.flag-als_richtige_antwort_markieren').show();
			$(this).parent().parent().parent().removeClass('flagged');
			$(this).parent().parent().parent().parent().parent().removeClass('rightAnswer');
		}
		else {
			var clicked = $(this).parent().parent().parent();
			$(clicked).addClass('flagged');
			$('li.flag-als_richtige_antwort_markieren').not(clicked).hide();
			$(clicked).parent().parent().addClass('rightAnswer');
		}

	});
	
	$(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
		
		location.reload();
		$('li.flag-als_richtige_antwort_markieren').find('a').click(function(){

		if ($(this).hasClass('unflag-action')) {
			$('li.flag-als_richtige_antwort_markieren').show();
			$(this).parent().parent().parent().removeClass('flagged');
			$(this).parent().parent().parent().parent().parent().removeClass('rightAnswer');
		}
		else {
			var clicked = $(this).parent().parent().parent();
			$(clicked).addClass('flagged');
			$(clicked).parent().parent().addClass('rightAnswer');
			$('li.flag-als_richtige_antwort_markieren').not(clicked).hide();
		}

		});
		
		
	});
});





