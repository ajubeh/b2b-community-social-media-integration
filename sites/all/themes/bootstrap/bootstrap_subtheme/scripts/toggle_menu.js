jQuery(document).ready(function($) {
	var toggle = 0;
	$('#block-system-main-menu ul.menu li:nth-child(3)').click(function(){		
		
		if(toggle == 0) {
		$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css({
			display : 'block', 
			opacity : 0
		});
		$(this).addClass('activemenu');
		setTimeout(function() {$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css('opacity', '1');}, 1);
		toggle = 1;
		return false;
		
		} else if(toggle == 1) {
			$(this).removeClass('activemenu');
			$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css({
				opacity : 0
			});
			setTimeout(function() {$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css('display', 'none');}, 500);
			toggle = 0;			
			return false;
			
		}
		
	});
	
	$(document).click(function(event) {
		var target = $(event.target);
		if( !target.is('#superfish-1 > li') && toggle == 1) {
			toggle = 0;
			$('#block-system-main-menu ul.menu li:nth-child(3)').removeClass('activemenu');
			$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css({
				opacity : 0
			});
			setTimeout(function() {$('.wrapper > .header-menu .region-header-main-menu .block-superfish').css('display', 'none');}, 500);
		}

	});

});
