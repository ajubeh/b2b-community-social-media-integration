/*!
 * Bootstrap v2.3.2
 *
 * Adapted through drupal-project-team
 *
 */

 /*
#######################################################################
#######################################################################
 */

/*Adapted Styles*/

/*Font-Styles
 * Telekom-Webfonts eingebunden und verwendbar
 */
@font-face {
  font-family: "TeleGroteskNormal";
  src: url("../fonts/TeleGroteskNormal.eot");
  src: local("#"), url("../fonts/TeleGroteskNormal.woff") format("woff"), 
  url("../fonts/TeleGroteskNormal.ttf") format("truetype"), 
  url("../fonts/TeleGroteskNormal.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleGroteskFett";
  src: url("../fonts/TeleGroteskFett.eot");
  src: local("#"), url("../fonts/TeleGroteskFett.woff") format("woff"), 
  url("../fonts/TeleGroteskFett.ttf") format("truetype"), 
  url("../fonts/TeleGroteskFett.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleGroteskHalbfett";
  src: url("../fonts/TeleGroteskHalbfett.eot");
  src: local("#"), url("../fonts/TeleGroteskHalbfett.woff") format("woff"), 
  url("../fonts/TeleGroteskHalbfett.ttf") format("truetype"), 
  url("../fonts/TeleGroteskHalbfett.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleGroteskHeadlineRegular";
  src: url("../fonts/TeleGroteskHeadline-Regular.eot");
  src: local("#"), url("../fonts/TeleGroteskHeadline-Regular.woff") format("woff"), 
  url("../fonts/TeleGroteskHeadline-Regular.ttf") format("truetype"), 
  url("../fonts/TeleGroteskHeadline-Regular.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleGroteskHeadlineUltra";
  src: url("../fonts/TeleGroteskHeadline-Ultra.eot");
  src: local("#"), url("../fonts/TeleGroteskHeadline-Ultra.woff") format("woff"), 
  url("../fonts/TeleGroteskHeadline-Ultra.ttf") format("truetype"), 
  url("../fonts/TeleGroteskHeadline-Ultra.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleGroteskUltra";
  src: url("../fonts/TeleGroteskUltra.eot");
  src: local("#"), url("../fonts/TeleGroteskUltra.woff") format("woff"), 
  url("../fonts/TeleGroteskUltra.ttf") format("truetype"), 
  url("../fonts/TeleGroteskUltra.svg#webfont1eYVxKc8") format("svg");
}

@font-face {
  font-family: "TeleLogo";
  src: url("../fonts/TeleLogo.eot");
  src: local("#"), url("../fonts/TeleLogo.woff") format("woff"), 
  url("../fonts/TeleLogo.ttf") format("truetype"), 
  url("../fonts/TeleLogo.svg#webfont1eYVxKc8") format("svg");
}

/*
#######################################################################
#######################################################################
*/

/*Styles for all sites, general sites
* Styles for subsites add at the end of this file, please
* Ganz oben: allgemeinste Styles;
* Danach folgen Styles die für alle Seiten gelten
* Unten Seitenspezifische Styles
*/ 

/*
#######################################################################
#######################################################################
* Meta-Styles
*/

*:before,
*:after {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

/*
#######################################################################
#######################################################################
* Allgemeine Styles der Html-Elemente
*/

html, body, div, span, p, 
a, header, footer, aside, section, 
h1, h2, h3, h4, h5, 
h6, thead, tr, td, ul,
ol {
	vertical-align: top;
}

html {
	padding-bottom: 12.5px;
	padding-top: 0px;
}

body {
	width: 98%;
	padding-top: 0px;
	color: #000;
	font-size: 13px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	line-height: 15.6px;
	margin: auto;
}

h2,
h3 {
	display: block;
	font-size: 1.5em;
	-webkit-margin-before: 0.83em;
	-webkit-margin-after: 0.83em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	font-weight: bold;
}

div {
	display: block;
}

a {
	font-size: 15px;
	font-family: TeleGrotskUltra, Arial, sans-serif;
	text-decoration: none;
	color: #000;
}

a:active {
	color: #e20074;
}

a:hover, 
a:focus {
	color: #e20074;
	cursor: pointer;
	text-decoration: none;
}

a:active, 
a:hover {
	outline: 0px;
}

a:-webkit-any-link {
	color: webkit-link;
	cursor: auto;
}

img {
	border: 0;
}

ul {
	padding: 0px;
	display: block;
	list-style-type: disc;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	-webkit-padding-start: 40px;
}

ul, ol {
	margin-top: 0px;
	margin-bottom: 10px;
}

li {
	display: list-item;
	text-align: -webkit-match-parent;
}

li.leaf {
	text-align: center;
}

/*
#######################################################################
#######################################################################
*/

ul li {
	list-style: none;
	list-style-position: outside;
	list-style-type: none;
}

.nav > li {
	position: relative;
	display: block;
}

.nav > li > a {
	position: relative;
	display: block;
	padding: 10px 15px;
}

/*
#######################################################################
#######################################################################
*/

.wrapper{
	width: 100%;
	max-width: 1280px;
	padding-top: 15.5px;
	margin: auto;
	background:#fff;
	position: relative;
	padding-bottom: 15.5px;
}

/*
#######################################################################
#######################################################################
* Styles für den Kopfbereich des Headers
*/

.wrapper .header{
	width: 100%;
	margin-bottom: 21px; 
}

.wrapper .header .header-top{
	height: 86px;
	margin-bottom: 15px;
	padding-left: 15px;
}

.wrapper .header .header-top .company-logo{
	max-height:86px;
	width: 160px;
	display: inline-block;
	float: left;
}

.wrapper .header .header-top .company-logo a {
	font-size: 15px;
	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
	text-decoration: none;
}

.wrapper .header .header-top .company-logo a img {
	display: block;
	height: auto;
	max-width: 100%;
	margin: 0px;
}

.wrapper .header .header-top .company-logo a p.logo-subline{
	padding-top: 26px;
	font-size: 15px;
	margin-bottom: 10px;
	vertical-align: top;
	display: block;
	width: 100%;
	height
}

.wrapper .header .header-top .header-links{
	height:16px;
	display: inline-block;
	float: right !important;
	padding-top: 25px;
	font-size: 13px;
	line-height: 15.6px;
	color: #000;
}

.wrapper .header .header-top .header-links .region-header-head {
	padding: 0px;
	margin-top: 0px;
	margin-bottom: 10px;
	font-size: 13.5588235294411764px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	color: #000;
	line-height: 16.270588235294117px;
	text-align: -webkit-match-parent;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	-webkit-padding-start: 40px;
}

.wrapper .header .header-top .header-links .region-header-head a {
	font-size: 14px;
	font-family: TeleGroteskHalbfett, Arial, sans-serif;

}
/*
#######################################################################
* Styles für den Imagebereich des Headers
* Erster Block (auskommentiert) ist für Headerimage
* Zweiter Block Styling ohne Headerimage
*/
/*
.front .wrapper .header .header-main {
	width: 100%;
	height: 600px;
	margin-bottom: 15px;
	background-image: url('../images/header/header.png');
	background-position: center;
	background-size: 100% auto;
	background-repeat: no-repeat;
}
*/
.wrapper .header .header-main {
	width: 100%;
	height: 5px;
	margin-bottom: 10px;
}

/*   
#######################################################################
* Styles für das Hauptmenü
* Dropdown Menü
*/

.wrapper .header .header-menu{
	margin-left: -0.9524%;
	margin-right: -0.9524%;
	height: 52px;
}

.wrapper .header .header-menu .region-header-main-menu {
	padding-left: 0.9524%;
	padding-right: 0.9524%;
	min-height: 1px;
	position: relative;
}

.wrapper .header .header-menu .region-header-main-menu,
.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu {
	height: 52px;
	text-align: left;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu {
	background-color: transparent;
	border: 1px solid #dadada;
	border-left: 0px;
	border-right: 0px;
	border-radius: 0px;
	padding-left: 0.65em;
	padding-right: 0.65em;
	position: relative;
	z-index: 1000;
	min-height: 50px;
	margin-bottom: 20px;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu .block-title {
	display: none;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav {
	display: table;
	width: 97.656%;
	float: left;
	margin: 0px;
	padding-left: 0px;
	list-style: none;
	padding-left: 1.172%;
	padding-right: 1.172%;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li {
	display: table-cell;
	float: none;
	text-align: center;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li a{
	font-size: 14px;
	font-family: TeleGroteskHeadlineRegular, Arial, sans-serif;
	padding: 16px 0px;
	color: #777;
	line-height: 20px;
}

/*
* Styles für die Listenelemente
* Aktives hervorgehoben
* Weitere ändern sich beim hovern, focus
*/

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li.active a.active,
.front .wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li.active a.active:hover,
.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li.active a.active:focus {
	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
	color: #e20074;
	background-color: #fff;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li a:hover,
.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li a:focus {
	font-family: TeleGroteskHeadlineRegular, Arial, sans-serif;
	color: #fff;
	background-color: #e20074;
}

.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li.active a.active:hover,
.wrapper .header .header-menu .region-header-main-menu #block-system-main-menu ul.menu.nav li.active a.active:focus {
	background-color: #e20074;
	color: #fff;
}

/*
#######################################################################
#######################################################################
* Styles für die Suche
* Ziel: Suche soll prominetnt sein;
* Erhält ein Aquarell (Graustufen)
*/

.wrapper .suche.wrapper {
	width: 100%;
	margin: auto;
	margin-bottom: 21px;
	background-image: url('../images/header/hintergrund.png');
	background-position: center;
	background-size: 99% 100%;
	background-repeat: no-repeat;
	/*background: #333;*/
	height: 80px;
	display: block;
	padding-top: 20px;
	padding-bottom: 30px;
}

.wrapper .suche.wrapper > .region-header-search {
	height: 100px;
	padding-top: 30px;
	padding-bottom: 30px;
	padding-left: 4.90%;
	padding-right: 4.90%;
	display: block;
	position: relative;
	width: 90%;
}

.wrapper .suche.wrapper > .region-header-search .block-search {
	height: 90px;
	padding-top: 5px;
	display: block;
	position: relative;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inlne {
	padding: 0px;
}


.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline div.input-append {
	height: 24px;
	margin-bottom: 33.5px !important;
	float: left;
	width: 50%;
	margin-left: 12%;
	margin-right: 10%;
	margin-top: -13px;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inlne div.input-append input {
	padding-left: 2%;
	padding-right: 2%;
	line-height: 24px;
	vertical-align: middle;
	font-size: 14px;
	font-family: TeleGroteskHeadlineRegular, Arial, sans-serif;
	color: #777;
	min-height: 20px !important;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form{
	width: 90%;
	margin-left: 5% !important;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline div.input-append button.btn {
	height: 24px;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline div.form-type-select {
	width: 80%;
	margin-left: 9.9%;
	margin-right: 9.9%;
	padding-left: 15%;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline div.form-type-select label.control-label{
	display: inline-block;
	width: 10%;
	min-width: 100px;
	margin-right: 0.5%;
	margin-bottom: 0;
	font-size: 15px;
	font-weight: 100;
	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
	color: #fff;
	line-height: 30px;
	vertical-align: middle;
}

.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline div.form-type-select div.controls {
	display: inline-block;
	width: 57%;
	height: 27px;
}


/*
#######################################################################
#######################################################################
* Button im Suchbereich zum Suchen oder Posten
*/
.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline button#edit-submit2,
 .wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline button#edit-submit3 {
	padding: 4.8px 32px;
	background: #777;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
	box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
	font-size: 15px;
	color: #fff;
	text-shadow: none;
	line-height: 20px;
	text-align: center;
	vertical-align: middle;
	cursor: pointer;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	font-style: normal;
	margin-top: -15px;
	margin-bottom: 20px;
	width: 120px;
}


.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline button#edit-submit2:hover,
 .wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline button#edit-submit3:hover {
 	background: #e20074;
 	text-decoration: none;
 }


 /*Breite der Suchleiste*/
.wrapper .suche.wrapper > .region-header-search .block-search .search-form div .container-inline .input-append input {
	width: 80%;
	display: inline-block;
	height: 14.9px;
}



/*
#######################################################################
#######################################################################
* Styles für die Forenübersicht (als Dropdownmenu)
* Ziel: Soll Navigation zu wichtigsten Inhaltsbereichen (Foren) erleichtern
*/

.wrapper .header-menu .region-header-main-menu section.block-superfish {
	position: absolute;
	width: 98.0952%;

	margin-top: -21px;
	background: white;
	z-index: 100;
	display: none;
	transition: opacity 0.5s;

}
.wrapper .header-menu .region-header-main-menu section.block-superfish h2 {
	display:none;
	font-size: 35px;
 	color: #000;
 	line-height: 35px;
 	margin: 0 0 25px;
 	margin-left: 1%;
 	padding: 0;
 	text-transform: uppercase;
 	font-family: TeleGroteskHeadlineRegular, Arial, sans-serif;
}
 
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu {
	width: 100%;
	padding: 0;
	margin: 0;
	border-top: 1px solid #dadada;
	border-bottom: 1px solid #dadada;
	display: block;
	height:100%;
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 {
	background: #fff;
	display: inline-block;
	float: none;
	text-align: center;
	width: 25%;
	height:100%;

}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1:nth-child(1) {
	/*background: url('../images/foren/technologie_b.png');*/
	background-repeat: no-repeat;
	background-size: 95% auto;
	background-position-y: 51px;
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1:nth-child(2) {
	/*background: url('../images/foren/intranet_b.png');*/
	background-repeat: no-repeat;
	background-size: 95% auto;
	background-position-y: 51px;	
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1:nth-child(3) {
	/*background: url('../images/foren/communitys_b.png');*/
	background-repeat: no-repeat;
	background-size: 95% auto;
	background-position-y: 51px;	
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1:nth-child(4) {
	/*background: url('../images/foren/collaboration_b.png');*/
	background-repeat: no-repeat;
	background-size: 95% auto;
	background-position-y: 51px;	
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1:nth-child(5) {
	/*background: url('../images/foren/collaboration_b.png');*/
	background-repeat: no-repeat;
	background-size: 95% auto;
	background-position-y: 51px;	
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1 {
	text-align: center;
	background: white;
	border:none;
	color: #777;
	word-wrap:break-word;
	padding-right: 5%;
	padding-left: 5%;
	padding-top:18.75px;
	padding-bottom:18.75px;
	border-bottom:1px solid #dadada;		
	margin-top: -1px;

}

@media all and (min-width: 860px){
	.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1 {
		
		text-align: center;
		background: white;
		border: none;
		color: #777;
		word-wrap:break-word;
		padding-right: 5%;
		padding-left: 5%;
		margin-top: -1px;
		border-bottom:1px solid #dadada;
	}
}

@media all and (min-width: 1140px){

	.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1 {
		
		text-align: center;
		background: white;
		border: none;
		color: #777;
		word-wrap:break-word;
		padding-right: 5%;
		padding-left: 5%;
		margin-top: -1px;
		border-bottom:1px solid #dadada;
	}

}
.activemenu, .activemenu > a {
	background: #e20074;
	color: white !important;
}
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1:hover,
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1:active,
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 a.sf-depth-1:focus {
	background: #e20074;
	color: #fff;
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 li.sf-depth-2 {
	border-bottom: 0px solid #dadada;
	
}


.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 li.sf-depth-2 a.sf-depth-2 {
	background: #fff;
	color: #777;
	border: none;
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 li.sf-depth-2 a.sf-depth-2:hover,
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 li.sf-depth-2 a.sf-depth-2:active,
.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 li.sf-depth-2 a.sf-depth-2:focus {
	background: #e20074;
	color: #fff;
}

.wrapper .header-menu .region-header-main-menu section.block-superfish ul.menu li.sf-depth-1 ul {
	margin-top: 25px;
	margin-bottom: 7px;
	max-width: 100%;
	min-width: 100%;
}

/*
#######################################################################
#######################################################################
* Styles für den Inhaltsbereich
* Ziel: Nur Grundgerüst
* genaue Ausgestaltung in startseite.css und speziellen Subpages festlegen.
*/

.wrapper .main-page.wrapper {
	width: 73.516%;
	display: inline-block;
	float: left;
	margin: 0;
	margin-bottom: 70px;
	padding: 0px;
	margin: auto;
}

.wrapper .main-page.wrapper .main-page-content.content-wrapper {
	width: 100%;
	display: block;
	margin: 0;
}

.wrapper .main-page.wrapper .main-page-content.content-wrapper .content-top,
.wrapper .main-page.wrapper .main-page-content.content-wrapper .content-main-content,
.wrapper .main-page.wrapper .main-page-content.content-wrapper .content-bottom {
	display: block;
	margin-bottom: 17px;
}


/*
#######################################################################
#######################################################################
* Styles für die Sidebar hier
* Ziel: Nur Grundgerüst;
* genaue Ausgestaltung in startseite.css und speziellen Subpages festlegen.
*/

.wrapper .sidebar.wrapper {
	width: 22.656%;
	margin: auto;
	display: inline-block;
	float: right;
	margin: 0;
	margin-bottom: 70px;
	padding: 0px;
}

/*
#######################################################################
#######################################################################
* Styles für den Footer hier
* Ziel: Nur Grundgerüst;
* genaue Ausgestaltung in startseite.css und speziellen Subpages festlegen.
*/

.wrapper .footer.wrapper {
	clear: both;
	display: block;
	width: 100%;
	position: relative;
	min-height: 1px;
	border: none;
	margin: 0;
	padding: 0;
	background-image: url('../images/header/hintergrund.png');
	background-position: 150% 50%;
	background-size: 60% 90%;
	background-repeat: no-repeat; 
}

.wrapper .footer.wrapper .footer-content {
	border-top: 1px solid #dadada;
	padding: 55px 35px 0;
}

/*
#######################################################################
 * Styles für die content-row + Metatags
 */

.wrapper .footer.wrapper .footer-content .footer-content-row {
	margin-right: -15px;
	margin-left: -15px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrapper .footer.wrapper .footer-content .footer-content-row:before {
	display: table;
	content: " ";
}

.wrapper .footer.wrapper .footer-content .footer-content-row:after {
	display: table;
	content: " ";
	clear: both;
}

/*
#######################################################################
 * Styles für die drei Inhaltsbereiche des Footers
 * Content für social-likes-block und footer-link-block noch einfügen
 */

/*
 * Styles für den linken Inhaltsbereich des Footers
 */

.wrapper .footer.wrapper .footer-content .footer-content-row .social-likes-block{
	width: 25%;
	float: left;
	position: relative;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-likes-block .social-likes-block-title {
	margin: 0.7em 0 2.5em;
	min-height: 35px;
	text-transform: uppercase;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-size: 15px;
	line-height: 90%;
	font-weight: 500;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-likes-block .social-likes-block-info {
	font-size: 15px;
	margin: 0 0 10px;
	margin-bottom: 10px;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
}

/*
 * Styles für den mittleren Inhaltsbereich des Footers
 */

.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block{
	text-align: center;
	width: 50%;
	float: left;
	position: relative;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block .social-media-block-title {
	margin: 0.7em 0 2.5em;
	min-height: 35px;
	text-transform: uppercase;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-size: 15px;
	line-height: 90%;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	text-align: center;
	font-weight: 500;
}


.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block .social-media-block-icons {
	margin-bottom: 15px;
	text-align: center;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block .social-media-block-icons a {
	display: inline-block;
	margin-right: 15px;
	font-size: 15px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	color: #000;
	text-decoration: none;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block .social-media-block-icons a:last-child {
	margin: 0;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .social-media-block .social-media-block-icons a img {
	display: block;
	height: auto;
	width: 40px;
}

/*
 * Styles für den rechten Inhaltsbereich des Footers
 */

.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block{
	width: 25%;
	float: left;
	position: relative;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block .footer-link-block-title { 
	margin: 0.7em 0 2.5em;
	min-height: 35px;
	text-transform: uppercase;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-size: 15px;
	line-height: 90%;
	font-weight: 500;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	-webkit-padding-start: 0px;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block ul.footer-link-block-info li a {
	font-size: 15px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
}

.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block ul.footer-link-block-info li a:hover span,
.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block ul.footer-link-block-info li a:active span,
.wrapper .footer.wrapper .footer-content .footer-content-row .footer-link-block ul.footer-link-block-info li a:focus span {
	color: #000;
	cursor: pointer;
	text-decoration: none;
 }

/*
#######################################################################
 */

.wrapper .footer.wrapper .footer-content-row .footer-link-list {
	width: 100%;
	position: relative;
	min-height: 1px;
	padding-left: 15px;
	padding-right: 15px;
	padding-top: 20px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrapper .footer.wrapper .footer-content-row .footer-link-list .region-footer section.block-system > *{
	float: left;
	margin-right: 6%;
	display: list-item;
	list-style: none;
	text-align: -webkit-match-parent;
	font-size: 13px;
	line-height: 15.6px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	color: black;
}

.wrapper .footer.wrapper .footer-content-row .footer-link-list .region-footer section.block-system > a {
	font-size: 15px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
}

.wrapper .footer.wrapper .footer-content-row .footer-link-list .region-footer section.block-system > * span {
	font-size: 15px;
	margin: 0 0 10px;
	margin-bottom: 10px;
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
}

.wrapper .footer.wrapper .footer-content-row .footer-link-list .region-footer section.block-system > * span a {
	font-size: 15px;
	font-family: TeleGroteskNormal, Arial, sans-serif;
}

/*
#######################################################################
#######################################################################
 * finish of file here
 */
