<?php

/**
 * @file
 * This file provides the jQuery Social Timeline drupal module functionality.
 */

/**
 * Implements hook_menu().
 */
function social_timeline_menu() {
  $items = array();

  $items['admin/config/services/social-timeline'] = array(
    'title' => 'Social Timeline',
    'description' => 'jQuery Social Timeline',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('social_timeline_admin_form'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

/**
 * Implements hook_libraries_info().
 */
function social_timeline_libraries_info() {
  $libraries['social-timeline'] = array(
    'name' => 'dpSocialTimeline Library',
    'vendor url' => 'http://codecanyon.net/item/jquery-social-timeline/2390758',
    'version arguments' => array(
      'file' => 'js/jquery.dpSocialTimeline.js',
      'pattern' => '@Social Timeline v([0-9\.a-z]+)@',
      'lines' => 5,
    ),
    'files' => array(
      'js' => array(
        'js/jquery.isotope.min.js',
        'js/jquery.dpSocialTimeline.js',
      ),
      'css' => array(
        'css/dpSocialTimeline.css',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_theme().
 */
function social_timeline_theme() {
  // Define our tabledrag theme.
  return array(
    'social_timeline_admin_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Return an array of available feeds in an array.
 *
 * @return array
 *   Array of available feeds.
 */
function _social_timeline_get_feeds() {
  // Define the default feeds.
  $feeds = array();

  $feeds['twitter'] = array('data' => 'Username', 'title' => 'Twitter');
  $feeds['twitter_hash'] = array('data' => 'Hashtag (without the #)', 'title' => 'Twitter Hashtag');
  $feeds['facebook_page'] = array('data' => 'Page ID', 'title' => 'Facebook');
  $feeds['instagram'] = array('data' => 'Username', 'title' => 'Instagram');
  $feeds['instagram_hash'] = array('data' => 'Hashtag (without the #)', 'title' => 'Instagram Hashtag');
  $feeds['delicious'] = array('data' => 'Username', 'title' => 'Delicious');
  $feeds['flickr'] = array('data' => 'User ID', 'title' => 'Flickr');
  $feeds['flickr_hash'] = array('data' => 'Hashtag (without the #)', 'title' => 'Flickr Hashtag');
  $feeds['tumblr'] = array('data' => 'Username', 'title' => 'Tumblr');
  $feeds['youtube'] = array('data' => 'Username', 'title' => 'Youtube');
  $feeds['youtube_search'] = array('data' => 'Search', 'title' => 'Youtube Search');
  $feeds['dribbble'] = array('data' => 'Username', 'title' => 'Dribbble');
  $feeds['digg'] = array('data' => 'Username', 'title' => 'Digg');
  $feeds['pinterest'] = array('data' => 'Username', 'title' => 'Pinterest');
  $feeds['vimeo'] = array('data' => 'Username', 'title' => 'Vimeo');

  return $feeds;
}

/**
 * Admin interface for the Social Time module.
 *
 * @return array
 *   Array form for the admin page.
 */
function social_timeline_admin_form($form, &$form_state) {
  // Set the limit drop down values.
  $limit = drupal_map_assoc(range(1, 100));

  // General settings.
  $form['social_timeline_global']['#tree'] = TRUE;
  $form['social_timeline_global']['#weight'] = -2;
  $form['social_timeline_global']['#collapsible'] = TRUE;
  $form['social_timeline_global']['#type'] = 'fieldset';
  $form['social_timeline_global']['#title'] = t('General Settings');

  // Get the global settings.
  $st_global = variable_get('social_timeline_global', array());

  // Set the general settings.
  $form['social_timeline_global']['skin'] = array(
    '#type' => 'select',
    '#title' => t('Skin'),
    '#options' => array(
      'light' => t('Light'),
      'dark' => t('Dark'),
    ),
    '#description' => t('Select the skin style'),
    '#default_value' => (isset($st_global['skin'])) ? $st_global['skin'] : 'light',
  );

  $form['social_timeline_global']['layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#options' => array(
      'timeline' => t('Timeline'),
      'columns' => t('Columns'),
      'one_column' => t('One Column'),
    ),
    '#description' => t('Select the layout mode'),
    '#default_value' => (isset($st_global['layout'])) ? $st_global['layout'] : 'timeline',
  );

  $form['social_timeline_global']['total'] = array(
    '#type' => 'textfield',
    '#title' => t('Total'),
    '#description' => t('Total number of items to retrieve'),
    '#default_value' => (isset($st_global['total'])) ? $st_global['total'] : 10,
    '#size' => 4,
  );

  $form['social_timeline_global']['items_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Items Width'),
    '#description' => t('Set the width of each item in the timeline (in pixels)'),
    '#default_value' => (isset($st_global['items_width'])) ? $st_global['items_width'] : 200,
    '#size' => 4,
  );

  $form['social_timeline_global']['colorbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Colorbox'),
    '#description' => t('Add colorbox support for images and videos'),
    '#default_value' => (isset($st_global['colorbox'])) ? $st_global['colorbox'] : 1,
  );

  $form['social_timeline_global']['social_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#description' => t('Set if you want to show the social icons'),
    '#default_value' => (isset($st_global['social_icons'])) ? $st_global['social_icons'] : 1,
  );

  $form['social_timeline_global']['show_filter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Filter'),
    '#description' => t('Set if you want to show the filter buttons'),
    '#default_value' => (isset($st_global['show_filter'])) ? $st_global['show_filter'] : 1,
  );

  $form['social_timeline_global']['show_layout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Layout'),
    '#description' => t('Set if you want to show the layout buttons'),
    '#default_value' => (isset($st_global['show_layout'])) ? $st_global['show_layout'] : 1,
  );

  $form['social_timeline_global']['show_share_buttons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "Share" Buttons'),
    '#description' => t('Set if you want to show the share buttons'),
    '#default_value' => (isset($st_global['show_share_buttons'])) ? $st_global['show_share_buttons'] : 1,
  );

  // Get the availalbe default feeds.
  $feeds = _social_timeline_get_feeds();

  // Add default feed form elements.
  $form['social_timeline']['#tree'] = TRUE;
  $form['social_timeline']['#weight'] = 10;
  $form['social_timeline']['#type'] = 'container';
  $st_feeds = variable_get('social_timeline', array());

  foreach ($feeds as $k => $v) {
    $form['social_timeline'][$k] = array(
      'feed' => array(
        '#markup' => $v['title'],
      ),
      'data' => array(
        '#type' => 'textfield',
        '#title' => check_plain($v['data']),
        '#default_value' => (isset($st_feeds[$k]['data'])) ? $st_feeds[$k]['data'] : NULL,
      ),
      'limit' => array(
        '#type' => 'select',
        '#title' => t('Limit'),
        '#options' => $limit,
        '#default_value' => (isset($st_feeds[$k]['limit'])) ? $st_feeds[$k]['limit'] : 5,
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => (isset($st_feeds[$k]['weight'])) ? $st_feeds[$k]['weight'] : 0,
        '#delta' => 10,
        '#title-display' => 'invisible',
      ),
      'active' => array(
        '#type' => 'checkbox',
        '#default_value' => (isset($st_feeds[$k]['active'])) ? $st_feeds[$k]['active'] : 0,
      ),
    );
  }

  // Get the custom feeds.
  $custom_feeds = variable_get('social_timeline');

  // Add the custom feeds to the form.
  if ($custom_feeds != NULL) {
    foreach ($custom_feeds as $k => $v) {
      if (strpos($k, 'custom_') !== FALSE) {
        // Title fix.
        $title = str_replace('custom_', '', $k);
        $title = ucwords(str_replace('_', ' ', $title));

        // Add custom feeds to the form array.
        $form['social_timeline'][$k] = array(
          'feed' => array(
            '#markup' => $title,
          ),
          'data' => array(
            '#type' => 'textfield',
            '#title' => check_plain($k),
            '#default_value' => (isset($st_feeds[$k]['data'])) ? $st_feeds[$k]['data'] : NULL,
          ),
          'limit' => array(
            '#type' => 'select',
            '#title' => t('Limit'),
            '#options' => $limit,
            '#default_value' => (isset($st_feeds[$k]['limit'])) ? $st_feeds[$k]['limit'] : 5,
          ),
          'icon' => array(
            '#type' => 'textfield',
            '#title' => t('Icon URL'),
            '#default_value' => (isset($st_feeds[$k]['limit'])) ? $st_feeds[$k]['icon'] : 5,
          ),
          'weight' => array(
            '#type' => 'weight',
            '#title' => t('Weight'),
            '#default_value' => (isset($st_feeds[$k]['weight'])) ? $st_feeds[$k]['weight'] : 0,
            '#delta' => 10,
            '#title-display' => 'invisible',
          ),
          'active' => array(
            '#type' => 'checkbox',
            '#default_value' => (isset($st_feeds[$k]['active'])) ? $st_feeds[$k]['active'] : 0,
          ),
          'delete' => array(
            '#type' => 'checkbox',
          ),
        );
      }
    }
  }

  // Sort the array based on row weight.
  uasort($form['social_timeline'], 'social_timeline_array_sort');

  // Add new feed form.
  $form['add_feed']['#tree'] = TRUE;
  $form['add_feed']['#type'] = 'fieldset';
  $form['add_feed']['#collapsible'] = TRUE;
  $form['add_feed']['#collapsed'] = TRUE;
  $form['add_feed']['#title'] = t('Add Custom Feed');

  $form['add_feed']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Feed Name'),
    '#description' => t('The name of the feed'),
  );

  $form['add_feed']['data'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The URL to the custom feed'),
  );

  $form['add_feed']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Feed Icon'),
    '#description' => t('The icon to represent the feed'),
  );

  $form['add_feed']['link'] = array(
    '#type' => 'button',
    '#value' => t('Add New Feed'),
    '#ajax' => array(
      'callback' => 'social_timeline_add_feed_callback',
      'event' => 'click',
    ),
  );

  // Add submit actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));

  return $form;
}

/**
 * Add new feed AJAX callback.
 *
 * @return array
 *   Array of ajax commands.
 */
function social_timeline_add_feed_callback($form, $form_state) {
  // Get the submitted values.
  $values = $form_state['values']['add_feed'];

  // Set the limit drop down values.
  $limit = array();
  for ($i = 1; $i <= 100; $i++) {
    $limit[$i] = $i;
  }

  // Add the custom feed to the form array.
  $k = drupal_strtolower(str_replace(' ', '_', $values['title']));
  $title = $values['title'];
  $icon = $values['icon'];
  $url = $values['data'];

  $form['social_timeline']['custom_' . $k] = array(
    'feed' => array(
      '#markup' => $title,
    ),
    'data' => array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#default_value' => $url,
      '#value' => $url,
    ),
    'limit' => array(
      '#type' => 'select',
      '#title' => t('Limit'),
      '#options' => $limit,
      '#default_value' => 5,
      '#value' => 5,
    ),
    'icon' => array(
      '#type' => 'textfield',
      '#title' => t('Icon'),
      '#default_value' => $icon,
      '#value' => $icon,
    ),
    'weight' => array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => 0,
      '#value' => 0,
      '#delta' => 10,
      '#title-display' => 'invisible',
    ),
    'active' => array(
      '#type' => 'checkbox',
      '#default_value' => 1,
      '#value' => 1,
    ),
    'delete' => array(
      '#type' => 'checkbox',
    ),
  );

  // Submit the data so we can save it.
  social_timeline_admin_form_submit($form, $form_state);

  // Re-process the form so we don't lose any values.
  $form_processed = form_builder($form['#form_id'], $form, $form_state);

  // Return the form via AJAX.
  $commands = array();
  $commands[] = ajax_command_html('#social-timeline-admin-form div:first-child', theme('social_timeline_admin_form', $form_processed));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Array sorting function for the Social Timeline admin form.
 *
 * @param array $a
 *   First element to compare against.
 * @param array $b
 *   Second element to compare against.
 *
 * @return bool
 *   Boolean for weighting.
 */
function social_timeline_array_sort($a, $b) {
  // Sort by item weight.
  if (is_array($a) && is_array($b)) {
    if ($a['weight']['#default_value'] == $b['weight']['#default_value']) {
      return 0;
    }

    return ($a['weight']['#default_value'] < $b['weight']['#default_value']) ? -1 : 1;
  }
}

/**
 * Array sorting function for the Social Timeline display.
 *
 * @param array $a
 *   First element to compare against.
 * @param array $b
 *   Second element to compare against.
 *
 * @return bool
 *   Boolean for weighting.
 */
function social_timeline_feeds_sort($a, $b) {
  // Sort by item weight.
  if ($a['weight'] == $b['weight']) {
    return 0;
  }

  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/**
 * Implements hook_block_info().
 */
function social_timeline_block_info() {
  $blocks['social_timeline'] = array(
    'info' => t('jQuery Social Timeline'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function social_timeline_block_view($delta = '') {
  switch ($delta) {
    case 'social_timeline':
      $block['subject'] = '<none>';
      $block['content'] = social_timeline_contents($delta);
      break;
  }

  return $block;
}

/**
 * Social Timeline block content callback.
 *
 * @param string $delta
 *   String containing the key name of the block.
 *
 * @return array
 *   Array of markup for the block.
 */
function social_timeline_contents($delta) {
  if ($delta == 'social_timeline') {
    // Load the library.
    $library = libraries_load('social-timeline');

    // Get the data.
    $global = variable_get('social_timeline_global');
    $feeds = variable_get('social_timeline');
    $custom_feeds = array();

    // Sort the feeds by weight.
    uasort($feeds, 'social_timeline_feeds_sort');

    // Format the feeds array.
    $feeds_settings = '';

    foreach ($feeds as $k => $v) {
      if ($v['active']) {
        if (strstr($k, 'custom_')) {
          $custom_feeds[$k] = $v;
        }
        else {
          $limit = (isset($v['limit'])) ? ', limit: \'' . $v['limit'] . '\'' : '';
          ($v['data'] != '') ? $feeds_settings .= "'" . $k . "': {data: '" . $v['data'] . "'" . $limit . "}, \n \t \t \t \t" : NULL;
        }
      }
    }

    // Format the custom feeds array.
    $custom_feed_settings = '';

    if (!empty($custom_feeds)) {
      $custom_feed_settings .= "custom: \n \t \t \t { \n \t \t \t \t";
      foreach ($custom_feeds as $k => $v) {
        $icon = (isset($v['icon'])) ? ', icon: \'' . $v['icon'] . '\'' : '';
        $limit = (isset($v['limit'])) ? ', limit: \'' . $v['limit'] . '\'' : '';
        $name = ", name: '" . $k . "'";
        ($v['data'] != '') ? $custom_feed_settings .= "'" . $k . "': {url: '" . $v['data'] . "'" . $limit . $icon . $name . "}, \n \t \t \t \t" : NULL;
      }
      $custom_feed_settings .= '},';
    }

    // Add jQuery Social Timeline initialization.
    drupal_add_js("jQuery(document).ready(function(){ jQuery('#socialTimeline').dpSocialTimeline({
      feeds:
      {
          " . $feeds_settings . "
      },
      " . $custom_feed_settings . "
      layoutMode: '" . $global['layout'] . "',
      addColorbox: " . $global['colorbox'] . ",
      showSocialIcons: " . $global['social_icons'] . ",
      showFilter: " . $global['show_filter'] . ",
      showLayout: " . $global['show_layout'] . ",
      share: " . $global['show_share_buttons'] . ",
      total: " . $global['total'] . ",
      itemWidth: '" . $global['items_width'] . "',
      skin: '" . $global['skin'] . "',
        });
     });", 'inline');

    // Return the HTML div that will contain the Social Timeline.
    return array('#markup' => '<div id="socialTimeline"></div>');
  }
}

/**
 * Theme callback for the Social Timeline admin form.
 *
 * @return string
 *   HTML table for the admin form.
 */
function theme_social_timeline_admin_form($variables) {
  $form = $variables['form'];
  $rows = array();

  // Put the form elements into table rows.
  foreach (element_children($form['social_timeline'], TRUE) as $id) {
    // Set the weight column class for tabledrag.
    $form['social_timeline'][$id]['weight']['#attributes']['class'] = array('social-item-weight');

    $rows[] = array(
      'data' => array(
        drupal_render($form['social_timeline'][$id]['feed']),
        drupal_render($form['social_timeline'][$id]['data']),
        drupal_render($form['social_timeline'][$id]['icon']),
        drupal_render($form['social_timeline'][$id]['limit']),
        drupal_render($form['social_timeline'][$id]['active']),
        drupal_render($form['social_timeline'][$id]['delete']),
        drupal_render($form['social_timeline'][$id]['weight']),
      ),

      // Add the draggable class to the rows for tabledrag.
      'class' => array('draggable'),
    );
  }

  // Set the table headers.
  $header = array(
    t('Feed'),
    t('Config'),
    t('Icon'),
    t('Limit'),
    t('Active?'),
    t('Delete'),
    t('Weight'),
  );

  // Set the table ID.
  $table_id = 'social-items-table';

  // Render the first part of the form before the table.
  $output = drupal_render_children($form);

  // Theme the form into a table.
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // Add a submit button at the bottom of the form.
  $output .= drupal_render($variables['form']['actions']);

  // Add the tabledrag functionality.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'social-item-weight');

  // Return the rendered HTML.
  return $output;
}

/**
 * Submit callback for the social_timeline_admin_form.
 */
function social_timeline_admin_form_submit($form, &$form_state) {
  // Determine if the form was submitted with AJAX.
  // If it was then add the new feed to the current
  // social timeline system variable.
  if ($form_state['triggering_element']['#value'] == 'Add New Feed') {
    // Get the current social timeline variable and add
    // our new custom one to it.
    $feeds = variable_get('social_timeline');
    $new_key = drupal_strtolower(str_replace(' ', '_', $form_state['values']['add_feed']['title']));
    $feeds['custom_' . $new_key] = array(
      'data' => $form_state['values']['add_feed']['data'],
      'limit' => 5,
      'weight' => 0,
      'active' => 1,
      'icon' => $form_state['values']['add_feed']['icon'],
      'title' => $form_state['values']['add_feed']['title'],
    );

    // Save the new social timeline variable.
    variable_set('social_timeline', $feeds);
  }
  else {
    // Loop through the form state and add/update our social timeline variable.
    foreach ($form_state['values'] as $id => $item) {
      if (strpos($id, 'social_timeline') !== FALSE) {
        variable_set($id, $item);
        $new_value = variable_get('social_timeline');
        if ($id == 'social_timeline') {
          foreach ($item as $k => $v) {
            // If the delete checkbox is checked then delete the custom feed.
            if (isset($form_state['values'][$id][$k]['delete'])) {
              if ($form_state['values'][$id][$k]['delete'] == 1) {
                unset($new_value[$k]);
              }
            }
          }

          // Save the new social timeline variable.
          variable_set('social_timeline', $new_value);
        }
        else {
          // Save the social timeline global settings variable.
          variable_set($id, $item);
        }
      }
    }
  }
}
