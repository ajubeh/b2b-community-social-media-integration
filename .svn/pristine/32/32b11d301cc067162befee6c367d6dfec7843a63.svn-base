/*!
 * Bootstrap v2.3.2
 *
 * Adapted through drupal-project-team
 * Hier nur Styles für die Sidbar aller Seiten einbinden
 * Allgemeine Styles (die für mehrere Unterseiten gelten) sollten in 
 * general.css einbinden
 *
 * Bitte Kommentieren und Strukturell darstellen bspw. mit Rauten abgrenzen.
 */

 /*
#######################################################################
#######################################################################
 * Styling allgemein Sidebar
 */

.wrapper .sidebar.wrapper aside{
 	text-align: left;
 	word-wrap: break-word;
 }


 /*
#######################################################################
#######################################################################
 * Styling allgemein Sidebar
 */
.wrapper .sidebar.wrapper aside section.block,
.wrapper .sidebar.wrapper section.tabs-for-content {
	margin-bottom: 4px;
	padding: 2px 3px;
	border-bottom: 4px double #dadada;
	font-size: 15px;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-weight: 150;
	text-decoration: none;
}

.wrapper .sidebar.wrapper aside section.block:last-child {
	border-bottom: none;
}

 /*
#######################################################################
 * Styling Wissen
 */

 .wrapper .sidebar.wrapper aside section#block-block-1 h2{
 	border-bottom: 1px solid #cecece;
 	padding-bottom: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
  	padding-left: 3px;
  	margin-top: 10px;
 }

  .wrapper .sidebar.wrapper aside section#block-block-1 p {
 	padding: 2.5px;
  } 

  .wrapper .sidebar.wrapper aside section#block-block-1 p:hover {
  	text-decoration: none;
  	background-color: #eeeeee;
  }


 /*
#######################################################################
 * Styling Usermenu
 */

 .wrapper .sidebar.wrapper aside section#block-system-user-menu h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-system-user-menu ul.menu.nav {
 	-webkit-padding-start: 0px;
 	padding: 0px;
  } 

.wrapper .sidebar.wrapper aside section#block-system-user-menu ul.menu.nav li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-system-user-menu ul.menu.nav li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
}

 /*
#######################################################################
 * Styling New Topics
 */

 .wrapper .sidebar.wrapper aside section#block-forum-new h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

   .wrapper .sidebar.wrapper aside section#block-forum-new ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	margin-left: 0pxM
  } 

.wrapper .sidebar.wrapper aside section#block-forum-new ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-forum-new ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper aside section#block-forum-new ul li a {
	padding: 0;
	line-height: 20px;
	padding-left: 1px;
}

.wrapper .sidebar.wrapper aside section#block-forum-new div.more-link {
	float: right;
	vertical-align: center;
	margin-bottom: 6px;
	padding-right: 2.5px;
	text-decoration: none;
}

.wrapper .sidebar.wrapper aside section#block-forum-new div.more-link:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

 /*
#######################################################################
 * Styling Highest User
 */

 .wrapper .sidebar.wrapper aside section#block-userpoints-0 h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	padding-left: 2.5px;
 	margin-bottom: 10px;
} 

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped thead tr th {
	padding: 0px;
	padding-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped thead tr th:first-child {
	float: left;
	padding-left: 2.5px;
	width: 70%;

}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped tbody {
	border: none;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped tbody tr.odd:hover,
.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped tbody tr.even:hover  {
	text-decoration: none;
	background: none !important;
  	background-color: #eeeeee !important;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped tbody tr td {
 	border: none;
 	line-height: 20px;
 	padding: 0px;
 	padding: 2.5px;
 	background: transparent;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 table.table-striped tbody tr td a {
	padding-left: 1px;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 ul li a {
	padding: 0;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 div.more-link {
	float: right;
	vertical-align: center;
	margin-bottom: 6px;
	padding-right: 2.5px;
	text-decoration: none;
}

.wrapper .sidebar.wrapper aside section#block-userpoints-0 div.more-link:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}


/*
#######################################################################
 * Meistbesuchte Foren
 */

 .wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view {
	-webkit-padding-start: 0px;
 	padding: 0px;
 	padding-left: 2.5px;
 	margin-bottom: 10px;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table thead tr th.views-field {
	padding: 0;
	border: none;
	border-color: transparent;
	padding-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table thead tr th.views-field:first-child{
	padding-left: 2.5px;
	width: 70%;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table tbody {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table tbody tr {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table tbody tr.odd:hover,
.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table tbody tr.even:hover {
	text-decoration: none;
	background: none !important;
  	background-color: #eeeeee !important;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.view div table tbody tr td {
	padding: 2.5px;
	border: none;
	line-height: 20px;

}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.more-link {
	float: right;
	vertical-align: center;
	margin-bottom: 6px;
	padding-right: 2.5px;
	text-decoration: none;
}

.wrapper .sidebar.wrapper aside section#block-views-meistbesuchte-foren-block div.more-link:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

/*
#######################################################################
 * Neueste Gruppen
 */

.wrapper .sidebar.wrapper aside section#block-views-neueste-gruppen-block h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

   .wrapper .sidebar.wrapper aside section#block-views-neueste-gruppen-block ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	margin-left: 0px;
  } 

.wrapper .sidebar.wrapper aside section#block-views-neueste-gruppen-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-views-neueste-gruppen-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper aside section#block-views-neueste-gruppen-block ul li a {
	padding: 0;
	line-height: 20px;
	padding-left: 1px;
}

/*
#######################################################################
 * User Login
 */

.wrapper .sidebar.wrapper aside section#block-user-login h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form {
	padding: 2px;
	margin-bottom: 4px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.fb_user-login-button-wrapper{
	width: 95%;
	padding: 4px;
	margin-left: 2.5%;
	margin-right: 2.5%;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	font-size: 20px;
	line-height: 24px;
	vertical-align: middle;
	padding-bottom: 10px;
	padding-top: 0px;
	padding-left: 1px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.form-type-textfield,
.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.form-type-password {
	margin-left: 0px;
	padding-left: 2.5px;
	padding-right: 2.5px;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-weight: normal;
	text-decoration: none;
	font-size: 15px;
	padding-bottom: 7px;
	line-height: 17.5px;
	margin-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.form-type-textfield label.control-label,
.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.form-type-password label.control-label {
	font-family:  TeleGroteskNormal, Arial, sans-serif;
	font-weight: normal;
	text-decoration: none;
	font-size: 15px;
	line-height: 17.5px;
	padding-left: 1px;
	padding-right: 1px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div ul {
	-webkit-padding-start: 0px;
 	padding: 0px;
 	margin-left: 0px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div ul li a {
	padding: 0;
	line-height: 20px;
	padding-left: 1px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.oneall_social_login {
	padding: 2.5px;
	margin-top: 5px !important;
	background-color: transparent;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.oneall_social_login div {
	background-color: #eeeeee !important;
	color: #000 !important;
	font-family: TeleGroteskNormal, Arial, sans-serif !important;
	font-size: 15px !important;
	line-height: 18px !important;
	vertical-align: middle !important;
	text-decoration: none !important;
	padding: 6px !important;
	font-weight: normal !important;
	padding-top: 2px !important;
	border: none !important;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.oneall_social_login div strong {
	line-height: 23px;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div div.oneall_social_login div a {
	color: #000 !important;
	font-family: TeleGroteskNormal, Arial, sans-serif !important;
	font-size: 15px !important;
	line-height: 18px !important;
	vertical-align: top !important;
	text-decoration: none !important;
	font-style: italic;
}

.wrapper .sidebar.wrapper aside section#block-user-login form#user-login-form div button.form-submit {
	width: 95%;
	padding: 4px;
	margin-left: 2.5%;
	margin-right: 2.5%;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	font-size: 20px;
	line-height: 24px;
	vertical-align: middle;
}

.wrapper .sidebar.wrapper aside section#block-fb-connect-login-facebook-for-drupal h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 }

.wrapper .sidebar.wrapper aside section#block-fb-connect-login-facebook-for-drupal > div.fb_not_connected div {
	width: 95%;
	padding: 4px;
	margin-left: 2.5%;
	margin-right: 2.5%;
	font-family: TeleGroteskNormal, Arial, sans-serif;
	font-size: 20px;
	line-height: 24px;
	vertical-align: middle;
	padding-bottom: 10px;
	padding-top: 0px;
	padding-left: 1px;
}

.wrapper .sidebar.wrapper aside section#block-fb-connect-login-facebook-for-drupal > div.fb_not_connected div * {
	width: 100% !important;
	position: relative;
}


/*
#######################################################################
 * More for this content
 */

 .wrapper .sidebar.wrapper section.tabs-for-content {
 	margin-bottom: 4px;
	padding: 2px 3px;
	border-bottom: 4px double #dadada;
	font-size: 15px;
	font-family: TeleGroteskUltra, Arial, sans-serif;
	font-weight: 150;
	text-decoration: none;
 }

.wrapper .sidebar.wrapper section.tabs-for-content h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper section.tabs-for-content ul.nav-tabs {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
  } 

.wrapper .sidebar.wrapper section.tabs-for-content ul.nav-tabs li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper section.tabs-for-content ul.nav-tabs li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section.tabs-for-content ul.nav-tabs li.active {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
	color: #e20074;
	text-decoration: none;
  	font-weight: bold;
}

.wrapper .sidebar.wrapper section.tabs-for-content ul.nav-tabs li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}

/*
 * Actionlink  im Forum nicht angezeigt, deshalb keine Höhe
 * Actionlink im Blog identisch wie andere Links
 */

.page-forum .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links {
 	height: 0px !important;
 	margin-bottom: 0px !important;
 	padding: 0px !important;
  } 

.page-blog .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	padding-bottom: 10px;
}

.page-blog .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
}

.page-blog .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.page-blog .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}

.page-blog .wrapper .sidebar.wrapper section.tabs-for-content ul.action-links li a i {
	padding-right: 0;
	display: none;
}


/*
#######################################################################
 * Popular Topics
 */

 .wrapper .sidebar.wrapper aside section#block-statistics-popular h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-statistics-popular h3 {
 	display: none;
 }

 .wrapper .sidebar.wrapper aside section#block-statistics-popular ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
  } 

.wrapper .sidebar.wrapper aside section#block-statistics-popular ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-statistics-popular ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-statistics-popular ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}


/*
#######################################################################
 * Languages
 */

.wrapper .sidebar.wrapper aside section#block-locale-language h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 	font-weight: normal;
}

.wrapper .sidebar.wrapper aside section#block-locale-language > ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
} 

ul.language-switcher-locale-url {
	margin-left: 0px;
}

.wrapper .sidebar.wrapper aside section#block-locale-language ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-locale-language ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-locale-language ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}

/*
#######################################################################
 * My Follows (Ansicht auf jeder Seite)
 */

.wrapper .sidebar.wrapper aside section#block-views-followers-zum-test-block-1 h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-views-followers-zum-test-block-1 * ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	margin-left: 0px;
  } 

.wrapper .sidebar.wrapper aside section#block-views-followers-zum-test-block-1 ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-followers-zum-test-block-1 ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-followers-zum-test-block-1 ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}

/*
#######################################################################
 * Aktivste Nutzer (Ansicht auf jeder Seite), letzter Monat
 */

.wrapper .sidebar.wrapper aside section#block-views-active-users-block h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-views-active-users-block * ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	margin-left: 0;
  } 

.wrapper .sidebar.wrapper aside section#block-views-active-users-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-active-users-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-active-users-block ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}


/*
#######################################################################
 * Neueste Forenthemen
 */

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block  * ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	margin-left: 0px
  } 

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-neuste-forentopics-block ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}

/*
#######################################################################
 * Meist diskutierte Forenthemen (letzter Monat)
 */

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block  ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
  } 

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-neuste-forentopics-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-neuste-forentopics-block ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}


/*
#######################################################################
 * Meistdiskutierte Foren
 */

 .wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view {
	-webkit-padding-start: 0px;
 	padding: 0px;
 	padding-left: 2.5px;
 	margin-bottom: 10px;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table thead tr th.views-field {
	padding: 0;
	border: none;
	border-color: transparent;
	padding-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table thead tr th.views-field:first-child{
	padding-left: 2.5px;
	width: 70%;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table tbody {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table tbody tr {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table tbody tr.odd:hover,
.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table tbody tr.even:hover {
	text-decoration: none;
	background: none !important;
  	background-color: #eeeeee !important;
}

.wrapper .sidebar.wrapper aside section#block-views-cf40e341f90172f27e366674b628fd17 div.view div table tbody tr td {
	padding: 2.5px;
	border: none;
	line-height: 20px;
}


/*
#######################################################################
 * Highest User 
 */

 .wrapper .sidebar.wrapper aside section#block-views-highest-users-block h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view {
	-webkit-padding-start: 0px;
 	padding: 0px;
 	padding-left: 2.5px;
 	margin-bottom: 10px;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table thead tr th.views-field {
	padding: 0;
	border: none;
	border-color: transparent;
	padding-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table thead tr th.views-field:first-child{
	padding-left: 2.5px;
	width: 70%;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table tbody {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table tbody tr {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table tbody tr.odd:hover,
.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table tbody tr.even:hover {
	text-decoration: none;
	background: none !important;
  	background-color: #eeeeee !important;
}

.wrapper .sidebar.wrapper aside section#block-views-highest-users-block div.view div table tbody tr td {
	padding: 2.5px;
	border: none;
	line-height: 20px;
}


/*
#######################################################################
 * Neueste Blogbeiträge
 */

.wrapper .sidebar.wrapper aside section#block-views-neueste-blogs-block h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

 .wrapper .sidebar.wrapper aside section#block-views-neueste-blogs-block * ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	margin-left: 0px !important;
  } 

.wrapper .sidebar.wrapper aside section#block-views-neueste-blogs-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-neueste-blogs-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-neueste-blogs-block ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}


/*
#######################################################################
 * Neueste Wissensbeiträge
 */

.wrapper .sidebar.wrapper aside section#block-views-neueste-wissensbeitr-ge-block h2 {
 	border-bottom: 1px solid #cecece !important;
 	margin-top: 10px !important;
 	padding-bottom: 3px !important;
 	padding-left: 3px !important;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif !important;
 	font-size: 15.5px !important;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-views-neueste-wissensbeitr-ge-block  ul {
 	-webkit-padding-start: 0px;
 	padding: 0px;
 	border-bottom: none;
 	margin-left: 0px;
  } 

.wrapper .sidebar.wrapper aside section#block-views-neueste-wissensbeitr-ge-block ul li {
	text-align: left;
	padding: 2.5px;
	line-height: 20px;
	width: 96%;
}

.wrapper .sidebar.wrapper aside section#block-views-neueste-wissensbeitr-ge-block ul li:hover {
	text-decoration: none;
  	background-color: #eeeeee;
}

.wrapper .sidebar.wrapper section#block-views-neueste-wissensbeitr-ge-block ul li a {
	padding: 0;
	padding-left: 1px;
	line-height: 20px;
	border: none;
	background-color: transparent;
}


/*
#######################################################################
 * Most visited
 */

 .wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block h2{
 	border-bottom: 1px solid #cecece;
 	margin-top: 10px;
 	padding-bottom: 3px;
 	padding-left: 3px;
 	font-family: TeleGroteskHeadlineUltra, Arial, sans-serif;
 	font-size: 15.5px;
 	font-weight: normal;
 }

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view {
	-webkit-padding-start: 0px;
 	padding: 0px;
 	padding-left: 2.5px;
 	margin-bottom: 10px;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table thead tr th.views-field {
	padding: 0;
	border: none;
	border-color: transparent;
	padding-bottom: 3px;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table thead tr th.views-field:first-child{
	padding-left: 2.5px;
	width: 70%;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table tbody {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table tbody tr {
	border: none;
	border-color: transparent;
	padding-left: 2.5px;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table tbody tr.odd:hover,
.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table tbody tr.even:hover {
	text-decoration: none;
	background: none !important;
  	background-color: #eeeeee !important;
}

.wrapper .sidebar.wrapper aside section#block-views-most-visitited-wissen-block div.view div table tbody tr td {
	padding: 2.5px;
	border: none;
	line-height: 20px;
}